import { Component } from "@angular/core";

import { products } from "src/app/products";

@Component ({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent {
  products = products

  share() {
    window.alert("Produto adicionado com sucesso")
  }

  onNotify() {
    window.alert("Você será notificado quando o produto estiver a venda")
  }

}
