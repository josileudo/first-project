import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms'

import { HttpClientModule } from '@angular/common/http';

import { CartService } from './cart.service';
import { AppComponent } from './app.component';
import { CartComponent } from './components/cart/cart.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { ShippingComponent } from './components/shipping/shipping.component';
import { ProductListComponent } from './components/product-list/produt-list.component';
import { ProductAlertsComponent } from './components/product-alerts/product-alerts.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: ProductListComponent},
      {path: 'products/:productId', component: ProductDetailsComponent},
      {path: 'cart', component: CartComponent},
      {path: 'shipping', component: ShippingComponent}
    ])
  ],

  declarations: [
    AppComponent,
    CartComponent,
    TopBarComponent,
    ShippingComponent,
    ProductListComponent,
    ProductAlertsComponent,
    ProductDetailsComponent,
  ],
  bootstrap: [AppComponent],
  providers: [CartService]
})
export class AppModule { }
